document.addEventListener('DOMContentLoaded', function() {
    const searchInput = document.getElementById('search-input');
    const resultsList = document.getElementById('results');

    // Representa los enlaces y sus respectivos datos
   const links = {
        'Volkswagen': { url:'https://www.volkswagen.com.ar/es.html', image: '/imagenes/volkswagen.png'},
        'Renault': { url:'https://www.renault.com.ar/', image: 'imagenes/renault.png'},
        'Peugeot' : { url:'https://www.peugeot.com.ar/', image: 'imagenes/peugeot-logo-1782772D87-seeklogo.com.png'},
        'Fiat' : { url:'https://www.fiat.com.ar', image: 'imagenes/Fiat.png'},
        'BMW' : { url:'https://www.bmw.com.ar/es/index.html', image:'imagenes/BMW   Logo.png'},
        'Mercedes benz' : { url:'https://www.mercedes-benz.com.ar', image: 'imagenes/mercedes-benz-logo.svg'},
        'Chevrolet' : { url:'https://www.chevrolet.com.ar', image: 'imagenes/Chevrolet.png'},
        'Toyota' : { url:'https://www.toyota.com.ar', image: 'imagenes/toyota6.png'},
        'Honda' : { url:'https://www.honda.com.ar', image: 'imagenes/hondalogo.png'},
        'Ford falcon' : { url:'https://www.ford.com.ar', image: 'imagenes/ford5.png'},


        
        // Agrega más enlaces según sea necesario
    };

    searchInput.addEventListener('input', function() {
        const query = searchInput.value.toLowerCase();
        const results = performSearch(query);
        displayResults(results);
    });

    function performSearch(query) {
        return Object.keys(links)
            .filter(link => link.toLowerCase().includes(query))
            .map(link => ({ title: link, url: links[link].url, image:links[link].image }));
    }

    function displayResults(results) {
        // Borra los resultados anteriores solo si hay una consulta válida
        if (searchInput.value.trim() === '') {
            resultsList.innerHTML = '';
            return;
        }

        // Muestra los nuevos resultados
        resultsList.innerHTML = '';
        results.forEach(result => {
            const li = document.createElement('li');
            const a = document.createElement('a');
            a.textContent = result.title;
        if(result.image) {
            const img = document.createElement ('img');
            img.src = result.image;
            img.alt = result.title;
            li.appendChild(img);
}
            a.href = result.url;
            a.target = "_blank";
            li.appendChild(a);
            resultsList.appendChild(li);
        });
    }
});